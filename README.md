# Team Statistics

This is a web app that connects to two instances of:

- Jira Cloud
- GitLab Cloud

and displays performance statistics for every team member.
